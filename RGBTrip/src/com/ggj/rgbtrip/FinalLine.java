package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class FinalLine extends RenderObject  {
	Texture texture;
	Sprite sprite;
	private SpriteBatch batch;
	float x;
	float y;
	
	public FinalLine(float x, float y){
		this.x = x;
		this.y = y;
		
		this.texture = new Texture(Gdx.files.internal("RGBTrip/Images/Finish_Tile.png"));
		this.sprite = new Sprite(texture);
		this.sprite.setPosition(x, y);
		this.sprite.setSize(GameManager.Instance.BlockSize*10, GameManager.Instance.BlockSize/2);
		
		batch = new SpriteBatch();
		
		System.out.println("start poistion " + x + " // " + y);
		System.out.println("start size" + GameManager.Instance.BlockSize*10 +"//"+ GameManager.Instance.BlockSize * 3);
	}
	
	public void renderSprite() {
		batch.begin();
		this.sprite.draw(batch);
		batch.end();	
	}

	@Override
	void render(ShapeRenderer shape) {
		// TODO Auto-generated method stub
		shape.end();
		this.renderSprite();
		shape.begin(ShapeType.Filled);
	}
	
	@Override
	protected void Move(float xOffset, float yOffset, float pDt)
	{
		this.x += xOffset/12;
		this.y += yOffset/12;
		
		this.sprite.setPosition(this.x, this.y);
	}

	@Override
	public Rectangle getAABB()
	{
		return new Rectangle(0,0,0,0);
	}
	
	@Override
	public void onCollision()
	{
		return;
	}
}
