package com.ggj.rgbtrip;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class RGBTrip implements ApplicationListener {
	private OrthographicCamera camera;
	private SpriteBatch spriteBatch;
	private ShapeRenderer shapeRenderer;
	private Board leftBoard;
	private Board rightBoard;
	private InputManager inputManager;
	private GameManager gameManager;
	private BitmapFont font;

	private SideBar leftSidebar;
	private SideBar rightSidebar;

	private float levelAcceleration;
	
	private SubliminalManager subliminal;
	boolean FinalLinePop = false;
	boolean GameIsStarted = false;
	boolean GameOver = false;
	ArrayList<Rectangle> whiteSeparators;
	
	StartMenu startMenu;
	EndMenu endMenu;
	LateralWord lateralWord;
	
	public enum Side
	{
		LEFT, RIGHT
	}
	
	@Override
	public void create()
	{
		this.fullScreen();
		
		float w =Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		
		this.gameManager = new GameManager();
		this.levelAcceleration = 2.5f;
				
		
		camera = new OrthographicCamera(1, h/w);
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		
		//Sidebars for hud
		Color sidebarColor = new Color(39/255f, 187/255f, 176/255f, 255/255f);
		leftSidebar = new SideBar(0, 0, w/10, h);
		leftSidebar.setColor(sidebarColor);
		rightSidebar = new SideBar(w - w/10, 0, w/10, h);
		rightSidebar.setColor(sidebarColor);
		
		//Boards
		Color leftBoardColor = GameManager.Instance.GetRandomColor();		
		leftBoard = new Board(w/10, 0, w/2 - w/10, h, leftBoardColor);
		leftBoard.setSide(Side.LEFT);
		rightBoard = new Board(w/2, 0, w/2 - w/10, h, GameManager.Instance.GetNextColor(leftBoardColor));
		rightBoard.setSide(Side.RIGHT);
		
		//Separation white lines
		float rectW = 6.0f;
		whiteSeparators = new ArrayList<Rectangle>();
		//left
		Rectangle rect = new Rectangle(leftSidebar.width - rectW/2,
				0,
				rectW,
				h);
		whiteSeparators.add(rect);
		
		//mid
		rect = new Rectangle(w/2 - rectW/2,
							0,
							rectW,
							h);
		whiteSeparators.add(rect);
		
		//right
		rect = new Rectangle(rightSidebar.x - rectW/2,
							0,
							rectW,
							h);
		whiteSeparators.add(rect);
		GameManager.Instance.BlockSize = leftBoard.width/10;
		
		this.inputManager = new InputManager();
		
		Pattern p = GameManager.Instance.CreatePattern();
		leftBoard.AddPattern(p);
		rightBoard.AddPattern(p);

		this.font = new BitmapFont(Gdx.files.internal("RGBTrip/Square-32.fnt"));
		
		subliminal = new SubliminalManager();
		
		this.startMenu = new StartMenu(this);
		this.endMenu = new EndMenu(this);
		this.lateralWord = new LateralWord();
	}
	
	@Override
	public void dispose() {
		spriteBatch.dispose();
		shapeRenderer.dispose();
	}

	@Override
	public void render() {
		if(this.GameOver)
		{
			this.renderEndGame();
		}
		else if(this.GameIsStarted)
		{
			this.renderPlay();
		}
		else
		{
			this.renderStart();
		}
	}

	public void renderEndGame ()
	{
		this.inputManager.CheckInputEndGame(this);
		
		this.endMenu.render();	
	}
	
	public void renderStart ()
	{
		this.inputManager.CheckStartInput(this);
		
		this.startMenu.render();
	}
	
	
	public static int currentDifficulty = 0;
	float finalTime = 0;
	
	public void StartGame (int difficulty)
	{
		this.currentDifficulty = difficulty;
		
		if(difficulty == 1)
		{
			this.finalTime = 15f;
			
			this.levelAcceleration = 2.5f;
			this.leftBoard.initialSpeed = 10f;
			this.rightBoard.initialSpeed = 10f;
			
			this.leftBoard.setScrollSpeed(10f);
			this.rightBoard.setScrollSpeed(10f);
		}
		else if(difficulty == 2)
		{
			this.finalTime = 12f;
			
			this.levelAcceleration = 4.0f;
			this.leftBoard.initialSpeed = 20f;
			this.rightBoard.initialSpeed = 20f;
			
			this.leftBoard.setScrollSpeed(20f);
			this.rightBoard.setScrollSpeed(20f);			
		}
		else if(difficulty == 3)
		{
			this.finalTime = 12f;
			
			this.levelAcceleration = 5.0f;
			this.leftBoard.initialSpeed = 25f;
			this.rightBoard.initialSpeed = 25f;
			
			this.leftBoard.setScrollSpeed(25f);
			this.rightBoard.setScrollSpeed(25f);				
		}
			
		this.GameIsStarted = true;
		GameManager.Instance.StartMusic();
	}
	
	public void renderPlay () 
	{		
		this.inputManager.CheckInput(this);
		GameManager.Instance.update(Gdx.graphics.getDeltaTime());

		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		shapeRenderer.begin(ShapeType.Filled);
			leftBoard.render(shapeRenderer);
			rightBoard.render(shapeRenderer);
        shapeRenderer.end();

        float duration = GameManager.Instance.maxTimer - GameManager.Instance.timer;//0 to 90
        float percent = duration / GameManager.Instance.maxTimer;//0 to 1
        float newScroll = rightBoard.initialSpeed + levelAcceleration * percent;
        
        leftBoard.setScrollSpeed(newScroll);
        rightBoard.setScrollSpeed(newScroll);


        //TODO: move this to legitimate class, resize boards
        spriteBatch.begin();
        	spriteBatch.setColor(1.0f);
        	this.font.setScale(3.3f);

		    font.draw(spriteBatch,
		    		""+Math.round(GameManager.Instance.timer),
    				Gdx.graphics.getWidth()/2 - font.getBounds("90").width/2,
		    		Gdx.graphics.getHeight() - font.getBounds("90").height/4);
        spriteBatch.end();
        
        leftSidebar.setScore(GameManager.Instance.scoreLeft);
        leftSidebar.render(shapeRenderer, spriteBatch);
        rightSidebar.render(shapeRenderer, spriteBatch);
        rightSidebar.setScore(GameManager.Instance.scoreRight);
        
        if(this.leftBoard.NeedAnotherPattern)
        {
        	this.leftBoard.NeedAnotherPattern = false;
        	this.rightBoard.NeedAnotherPattern = false;
        	
        	if(GameManager.Instance.timer <= this.finalTime && ! this.FinalLinePop)
        	{
        		this.FinalLinePop = true;
        		
        		FinalLine fl1 = new FinalLine(this.leftBoard.x, this.leftBoard.height);
        		FinalLine fl2 = new FinalLine(this.rightBoard.x, this.rightBoard.height);
        		
        		this.leftBoard.AddFinalLine(fl1);
        		this.rightBoard.AddFinalLine(fl2);
        	}
        	else if(GameManager.Instance.timer > this.finalTime)
        	{
        		Pattern p = GameManager.Instance.CreatePattern();
        		leftBoard.AddPattern(p);
        		rightBoard.AddPattern(p);           		
        	}
        	else if(GameManager.Instance.timer < 1f)
        	{
        		GameManager.Instance.StopMusic();
        		this.GameOver = true;
        		this.endMenu.SetVictory(GameManager.Instance.scoreLeft, GameManager.Instance.scoreRight);
        	}
        }
        
        shapeRenderer.begin(ShapeType.Filled);
        for(int i=0;i<whiteSeparators.size();i++)
        {
        	shapeRenderer.setColor(Color.WHITE);
        	shapeRenderer.rect(whiteSeparators.get(i).x,
        						whiteSeparators.get(i).y,
        						whiteSeparators.get(i).width,
        						whiteSeparators.get(i).height);
        }
        shapeRenderer.end();
        this.subliminal.render();
        
        this.lateralWord.render();
	}
	
	public void MoveStartMenu(int x, int y)
	{
		this.startMenu.Move(x, y);
	}
	
	public void MoveEndMenu(int x)
	{
		this.endMenu.Move(x);
	}
	
	public void PressEnterMenu()
	{
		this.startMenu.PressEnter();
	}
	
	public void PressEnterEndGame()
	{
		this.endMenu.PressEnter();
	}
	
	public void RestartGame()
	{
		this.leftBoard.Clear();
		this.rightBoard.Clear();		
		GameManager.Instance.timer = 90f;
		this.GameOver = false;
		this.GameIsStarted = false;
		GameManager.Instance.scoreLeft = 500f;
		GameManager.Instance.scoreRight = 500f;
		GameManager.Instance.StopMusic();
		this.leftBoard.speed = this.leftBoard.initialSpeed;
		this.rightBoard.speed = this.rightBoard.initialSpeed;		
		this.FinalLinePop = false;
		GameManager.Instance.SetGameColor();
		Color leftBoardColor = GameManager.Instance.GetRandomColor();
		leftBoard.setColor(leftBoardColor);
		this.rightBoard.setColor(GameManager.Instance.GetNextColor(leftBoardColor));
		this.leftBoard.ReSetShip();
		this.rightBoard.ReSetShip();
	}
	
	public void Escape ()
	{
		GameManager.Instance.StopMusic();
		RestartGame();
	}
	
	public void MoveShipOne(int x, int y)
	{
		//move ship on board one
		leftBoard.MovePlayer(x, y);
	}
	
	public void MoveShipTwo(int x, int y)
	{
		//move ship on board two
		rightBoard.MovePlayer(x, y);
	}
	
	public void NextColorOnBoard()
	{
		GameManager.Instance.PlayHitSpace();
		this.leftBoard.NextColor();
		this.rightBoard.NextColor();
		this.subliminal.SwitchColor();
	}
	
	public void DashShipOne (int x, int y)
	{
		this.leftBoard.DashShipOne(x, y);
	}
	
	public void DashShipTwo(int x, int y)
	{
		this.rightBoard.DashShipTwo(x, y);
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	public void fullScreen()
	{
		if(Gdx.graphics.supportsDisplayModeChange())
		{
			DisplayMode dekstop = Gdx.graphics.getDesktopDisplayMode();
			Gdx.graphics.setDisplayMode(dekstop);
		}
		else
		{
			System.out.println("Device can't change resolution");
		}
	}
}
