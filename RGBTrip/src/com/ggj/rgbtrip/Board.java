package com.ggj.rgbtrip;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.ggj.rgbtrip.RGBTrip.Side;

public class Board extends RenderObject {
	public float x, y, width, height;
	private Rectangle rect;
	private Ship ship;
	private float scrollSpeed;
	private ArrayList<RenderObject> blockList;
	private Side side;
	
	public float initialSpeed;
	
	
	public boolean NeedAnotherPattern = false;
	
	public Board(float pX, float pY, float pWidth, float pHeight, Color pInitColor){
		this.x = pX;
		this.y = pY;
		this.width = pWidth;
		this.height = pHeight;
		this.color = pInitColor;		
		this.scrollSpeed = this.initialSpeed = 15.0f;
		this.rect = new Rectangle(pX, pY, pWidth, pHeight);
		
		this.ship = new Ship(10);
		this.ship.setColor(Color.BLACK);
		
		if(RGBTrip.currentDifficulty == 1)
			this.ship.setSpeed(200f);
		else if(RGBTrip.currentDifficulty == 2)
			this.ship.setSpeed(275);
		else
			this.ship.setSpeed(350f);
		
		this.ship.setPosition(this.x + this.width/2, this.height/10);
		
		this.blockList = new ArrayList<RenderObject>();

	}
	
	public void ReSetShip()
	{
		this.ship.setPosition(this.x + this.width/2, this.height/10);
	}
	
	public void render(ShapeRenderer pShapeRenderer){
		//Updating pos of objects
		float dt = Gdx.graphics.getDeltaTime();
		for(int i=0;i<blockList.size();i++){
			blockList.get(i).Move(0, -scrollSpeed, dt);
		}
		
		//Testing collisions
		for(int i=0;i<blockList.size();i++)
		{
			if(blockList.get(i).getAABB().overlaps(ship.getAABB())
					&& !blockList.get(i).hasCollided)
			{
				ship.onCollision();
				blockList.get(i).onCollision();
			}
		}
		
		//Destroying out of screen objects 
		int j=0;
		while(j<blockList.size())
		{
			if(blockList.get(j).getAABB().y + blockList.get(j).getAABB().height < this.rect.y )
			{
				blockList.remove(j);
			}
			else
			{
				j++;
			}
		}
		
		
		//============
		//DRAW
		//============
		
		
		//Rendering background
		pShapeRenderer.setColor(this.color);
        pShapeRenderer.rect(this.x, this.y, this.width, this.height);
        
        //Rendering shapes
        for(int i=0;i<blockList.size();i++){
        	blockList.get(i).render(pShapeRenderer);
        }
        
        //Rendering ship
        ship.render(pShapeRenderer);
        NeedAnotherPattern = isAnotherBlockNeeded();
        
	}
	
	public void DashShipOne(int x, int y)
	{
		if(this.side == Side.LEFT)
		{
			this.ship.Dash(x, y);
		}
	}
	
	public void DashShipTwo(int x, int y)
	{
		if(this.side == Side.RIGHT)
		{
			this.ship.Dash(x, y);
		}
	}
	
	@Override
	public Rectangle getAABB()
	{
		return this.rect;
	}
	
	public void MovePlayer(float pX, float pY){
		float dt = Gdx.graphics.getDeltaTime();
		ship.Move(pX, pY, dt);
		
		Rectangle r = ship.getAABB();
		if(r.x < this.rect.x)//Too far left
		{
			ship.setPosition(this.rect.x, this.height/10);
		}
		else if((r.x + r.width) > (this.rect.x + this.rect.width -1 ))
		{
			ship.setPosition(this.rect.x + this.rect.width - r.width -1, this.height/10);
		}
		
		if(r.y < this.rect.y)
		{
			ship.setPosition(r.x, this.height/10);
		}
		else if((r.y + r.height) > (this.rect.y + this.height))
		{
			ship.setPosition(r.x, this.height/10);
		}
	}
	
	public boolean isAnotherBlockNeeded()
	{
		if(blockList.isEmpty())
			return true;
		
		RenderObject lastBlock = blockList.get(blockList.size()-1);
		
//		if(this.rect.contains(lastBlock.getAABB()))
		if(this.rect.y + this.rect.height > lastBlock.getAABB().y + lastBlock.getAABB().height)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setSide(Side pSide)
	{
		this.side = pSide;
		this.ship.setSide(this.side);
	}
	
	public void setBatch(SpriteBatch batch)
	{
		if(this.ship != null)
		{
			this.ship.setBatch(batch);
		}
	}
	
	//==========
	//Setters...
	//==========
	public void setScrollSpeed(float pScrollSpeed){
		this.scrollSpeed = pScrollSpeed;
		
		for(int i = 0; i < this.blockList.size(); i++)
		{
			this.blockList.get(i).speed = pScrollSpeed;
		}
	}

	@Override
	public void setColor(Color color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	@Override
	public void Move(float xOffset, float yOffset, float pDt) {
		return;
	}
		
	public void NextColor()
	{
		this.color = GameManager.Instance.GetNextColor(this.color);
	}


	@Override
	public void onCollision() 
	{
		// TODO Auto-generated method stub 
		return;
	}
	
	public void AddPattern(Pattern p)
	{
		int maxH = p.grid[0].length;
		
		for(int j = 0 ; j < maxH; j++)
		{
			for(int i = 0; i < 10; i++)
			{
				if(p.grid[i][j] == ShapeType.SQUARE)
				{
					SquareBlock block = new SquareBlock(i*GameManager.Instance.BlockSize + this.x, j*GameManager.Instance.BlockSize+  this.height, p.color[i][j]);
					block.setSpeed(this.scrollSpeed);
					this.blockList.add(block);
				}
				else if(p.grid[i][j] == ShapeType.CIRCLE) 
				{
					CircleBlock block = new CircleBlock(i*GameManager.Instance.BlockSize + this.x+ GameManager.Instance.BlockSize/2, j*GameManager.Instance.BlockSize+  this.height, p.color[i][j]);
					block.setSpeed(this.scrollSpeed);
					this.blockList.add(block);				
				}
				else if(p.grid[i][j] == ShapeType.TRIANGLE)
				{
					TriangleBlock block = new TriangleBlock(i*GameManager.Instance.BlockSize + this.x, j*GameManager.Instance.BlockSize+  this.height, p.color[i][j]);
					block.setSpeed(this.scrollSpeed);
					this.blockList.add(block);					
				}
				else if(p.grid[i][j] == ShapeType.RECTANGLE)
				{
					RectangleBlock block = new RectangleBlock(i*GameManager.Instance.BlockSize + this.x, j*GameManager.Instance.BlockSize+  this.height, p.rectangleSize[i][j] , p.rectanglePos[i][j], p.color[i][j]);
					block.setSpeed(this.scrollSpeed);
					this.blockList.add(block);					
				}
			}
		}
	}
	
	public void AddFinalLine(FinalLine f)
	{
		this.blockList.add(f);
	}
	
	public void Clear()
	{
		this.blockList.clear();
	}
}
