package com.ggj.rgbtrip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;


public class Pattern {
	
	String patternLD09;
	String patternLD09Color;
					  
	public ShapeType[][] grid;
	public Color[][] color;
	public int[][] rectangleSize;
	public int[][] rectanglePos;
	
	public Pattern()
	{		
		int index = MathUtils.random(0, GameManager.Instance.AllPaterns.size()-1);
		
		patternLD09 = PatternFileReader.readFile(Gdx.files.internal("RGBTrip/"+ GameManager.Instance.AllPaterns.get(index) + ".txt").file().getAbsolutePath());
		patternLD09Color = PatternFileReader.readFile(Gdx.files.internal("RGBTrip/"+ GameManager.Instance.AllPaterns.get(index) + "-color.txt").file().getAbsolutePath());
						
		//Create on square pattern;		
		String[] cells = patternLD09.split(",");
		String[] colors = patternLD09Color.split(",");
		
		Map<String,Color> savedColor = new HashMap<String,Color>();
		for(int c = 0; c < colors.length; c++)
		{			
			if( (colors[c] != "0") && !savedColor.containsKey(colors[c]))
			{
				savedColor.put(colors[c], GameManager.Instance.GetRandomColor());
			}
		}
				
		int height = cells.length / 10;
		
		grid = new ShapeType[10][height];		
		color = new Color[10][height];
		rectangleSize = new int[10][height];
		rectanglePos = new int[10][height];
		
		int i = 0;
		int j = height-1;
		int count = 0;
		
		while(count < cells.length)
		{			
			if(cells[count].charAt(0) == '1')
			{
				grid[i][j] = ShapeType.SQUARE;
			}
			else if(cells[count].charAt(0) == '2')
			{
				grid[i][j] = ShapeType.RECTANGLE;
				rectangleSize[i][j] = Integer.parseInt(cells[count].charAt(2)+"");
				rectanglePos[i][j]	= Integer.parseInt(cells[count].charAt(4)+"");
			}
			else if(cells[count].charAt(0) == '3')
			{
				grid[i][j] = ShapeType.CIRCLE;
			}
			else if(cells[count].charAt(0) == '4')
			{
				grid[i][j] = ShapeType.TRIANGLE;
			}
			
			
			if(i==9)
			{
				i = 0;
				j--;
			}
			else
			{
				i++;
			}
			
			count++;
		}
		
		count = 0;
		i=0;
		j=height-1;
		while(count < colors.length)
		{	
			if(!colors[count].equals("0"))
			{
				color[i][j] = savedColor.get(colors[count]);			
			}
			
			if(i==9)
			{
				i = 0;
				j--;
			}
			else
			{
				i++;
			}
			
			count++;
		}
		
		
		/*
		Color col = GameManager.Instance.GetRandomColor();
		
		int maxH = grid[0].length;
				
		for(j = 0 ; j < maxH; j++)
		{
			for(i = 0; i < 10; i++)
			{
				if(grid[i][j] != null)
				{
					color[i][j] =  GameManager.Instance.GetRandomColor();				
				}
			}
		}*/
	}
}
