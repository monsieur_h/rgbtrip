package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StartMenu {
	
	Sprite[][] Sprites;
	Texture[][] Textures;
	
	int i = 0;
	int j = 0;
	private SpriteBatch batch;
	private RGBTrip game;
	
	public StartMenu (RGBTrip game)
	{
		this.game = game;
		
		this.Sprites = new Sprite[10][10];
		this.Textures = new Texture[10][10];
		
		Texture.setEnforcePotImages(false);
		//START GAME
		this.Textures[0][0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/2_Start_Easy.png"));
		this.Textures[1][0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/3_Start_Normal.png"));
		this.Textures[2][0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/4_Start_Hard.png"));
		
		//HOW TO PLAY
		this.Textures[0][1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/5_HowToPlay.png"));
		this.Textures[1][1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/HTP-01.png"));
		this.Textures[2][1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/HTP-02.png"));
		this.Textures[3][1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/HTP-03.png"));
		
		//QUIT
		this.Textures[0][2] =  new Texture(Gdx.files.internal("RGBTrip/Images/Menue/6_Quit_Yes.png"));
		this.Textures[1][2] =  new Texture(Gdx.files.internal("RGBTrip/Images/Menue/7_Quit_No.png"));
		
		Texture.setEnforcePotImages(true);
		
		//START GAME
		this.Sprites[0][0] = new Sprite(this.Textures[0][0]);
		this.Sprites[1][0] = new Sprite(this.Textures[1][0]);
		this.Sprites[2][0] = new Sprite(this.Textures[2][0]);
		
		//HOW TO PLAY
		this.Sprites[0][1] = new Sprite(this.Textures[0][1]);
		this.Sprites[1][1] = new Sprite(this.Textures[1][1]);
		this.Sprites[2][1] = new Sprite(this.Textures[2][1]);
		this.Sprites[3][1] = new Sprite(this.Textures[3][1]);
		
		//QUIT
		this.Sprites[0][2] = new Sprite(this.Textures[0][2]);
		this.Sprites[1][2] = new Sprite(this.Textures[1][2]);
		
		for(int i = 0 ; i < this.Sprites.length; i++)
		{
			for(int j = 0; j < this.Sprites[i].length; j++)
			{
				if(this.Sprites[i][j] != null)
				{
					this.Sprites[i][j].setPosition(0, 0);
					this.Sprites[i][j].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				}
				else
				{
					this.Sprites[i][j] = null;
				}
			}
		}
		
		batch = new SpriteBatch();
	}
	
	public void Move(int x, int y)
	{
		if(x != 0)
		{
		if((i+x) >= 0 && (i+x) < this.Sprites.length && this.Sprites[i+x][j] != null) {
			i += x;
		}
		}
		else if(y != 0)
		{
		
		if((j+y) >= 0 && (j+x) < this.Sprites[i].length && this.Sprites[i][j+y] != null) {
			j += y;
		}
		}
	}
	
	public void PressEnter()
	{
		if(j == 0)
		{
			game.StartGame(i+1);
		} 
		else if(j == 1)
		{
			if(i < 3) {
				this.Move(1, 0);
			}
			else
			{
				i = 0;
			}
		}
		else if(j==2 && i == 0)
		{
			Gdx.app.exit();
		}
		else if(j==2 && i == 1)
		{
			i=0;
			j=0;
		}
	}
	
	public void render()
	{
		batch.begin();
		this.Sprites[i][j].draw(batch);
		batch.end();
	}
}
