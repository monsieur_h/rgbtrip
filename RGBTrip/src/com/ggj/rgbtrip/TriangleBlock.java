package com.ggj.rgbtrip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class TriangleBlock extends RenderObject {

	private float x, y;
//	private float[] vertices;
	
	public TriangleBlock(float pX, float pY, Color pColor){
		this.color = pColor;
		this.x = pX;
		this.y = pY;
		
		this.vertices = new float[6];
		
		this.vertices[0] = pX;
		this.vertices[1] = pY;
		
		this.vertices[2] = pX + GameManager.Instance.BlockSize / 2;
		this.vertices[3] = pY + GameManager.Instance.BlockSize;
		
		this.vertices[4] = pX + GameManager.Instance.BlockSize;
		this.vertices[5] = pY;
	}
	
	@Override
	void render(ShapeRenderer shape) {
		shape.end();
		shape.begin(ShapeType.Line);
			shape.setColor(this.color);
			shape.polygon(this.vertices);
		shape.end();
		shape.begin(ShapeType.Filled);		
	}
}
