package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LateralWord {
	Texture[] Textures;
	Sprite[] Sprites;
	private SpriteBatch batch;
	
	
	
	public LateralWord()
	{
		this.Textures = new Texture[7];
		this.Sprites = new Sprite[7];
		
		Texture.setEnforcePotImages(false);
		
		this.Textures[0] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/aweful.png"));
		this.Textures[1] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/useless.png"));
		this.Textures[2] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/bad.png"));
		this.Textures[3] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/okay.png"));
		this.Textures[4] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/great.png"));
		this.Textures[5] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/awesome.png"));
		this.Textures[6] = new Texture(Gdx.files.internal("RGBTrip/Images/LateralWord/marvelous.png"));
		
		Texture.setEnforcePotImages(true);
		
		for(int i = 0; i < 7; i++)
			this.Sprites[i] = new Sprite(this.Textures[i]);
		
		batch = new SpriteBatch();
	}
	
	public void render()
	{
		float scoreLeft = GameManager.Instance.scoreLeft;
		float scoreRight = GameManager.Instance.scoreRight;
		
		float ecart = scoreLeft - scoreRight;
		
		int left = 0;
		int right = 0;

		int gagnant = 0;
		int perdant = 0;
		if(Math.abs(ecart)<30)
		{
			gagnant = perdant = 3;
		}
		else if(Math.abs(ecart)<80)
		{
			gagnant = 4;
			perdant = 2;
		}
		else if(Math.abs(ecart)<150)
		{
			gagnant = 5;
			perdant = 1;
		}
		else if(Math.abs(ecart)>=150)
		{
			gagnant = 6;
			perdant = 0;
		}
		
		if(ecart<0)
		{
			left = perdant;
			right = gagnant;
		}
		else
		{
			left = gagnant;
			right = perdant;
		}
		batch.begin();
		
		this.Sprites[left].setPosition(0, -100);
		this.Sprites[left].draw(batch);
		
		this.Sprites[right].setPosition(Gdx.graphics.getWidth() - this.Sprites[6].getWidth(), -100);
		this.Sprites[right].draw(batch);
		
		batch.end();		
	}
}
