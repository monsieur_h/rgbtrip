package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class SideBar {
	public float x, y, width, height;
	private Color color;
	private float score;
	private BitmapFont font;
	private String text;
	
	private float effectDuration;
	private float effectElapsed;
	
	public SideBar(float x, float y, float width, float height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.font = new BitmapFont(Gdx.files.internal("RGBTrip/Square-32.fnt"));
		this.font.scale(0.5f);
		this.effectDuration = this.effectDuration = 0.25f;
	}
	
	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public void setScore(float score)
	{
		if(this.score != score)
			effectElapsed = 0;
		
		this.score = score;
	}
	
	public void render(ShapeRenderer shape, SpriteBatch batch)
	{
		int score = Math.round(this.score);
		shape.begin(ShapeType.Filled);
		
		float effectRatio = this.effectElapsed/this.effectDuration;
		
		Color myColor = new Color(this.color.r / effectRatio + this.color.r,
				this.color.g / effectRatio + this.color.g,
				this.color.b / effectRatio + this.color.b , 1f);
		shape.setColor(myColor);
		shape.rect(this.x, this.y, this.width, this.height);
		
		TextBounds bounds = this.font.getBounds(""+score);
		shape.end();
		
		batch.begin();
		font.draw(batch, ""+score,
				this.x + this.width/2 - bounds.width/2,
				this.y + bounds.height + 10);
		if(this.text != null && !this.text.isEmpty())
		{
			bounds = this.font.getBounds(this.text);
			font.draw(batch, this.text,
					this.x + this.width/2 - bounds.width/2,
					this.y + this.height - bounds.height/2);
		}
		batch.end();
		
		this.updateEffect(Gdx.graphics.getDeltaTime());
	}
	
	public void setText(String text)
	{
		this.text = text;
		String tmp = "";
		for(int i=0;i<this.text.length()-1;i=i++)
		{
			tmp += this.text.substring(i, i+1);
		}
	}
	
	private void updateEffect(float dt)
	{
		this.effectElapsed += dt;
		this.effectElapsed = (this.effectElapsed > this.effectDuration) ? this.effectDuration : this.effectElapsed;
	}
}
