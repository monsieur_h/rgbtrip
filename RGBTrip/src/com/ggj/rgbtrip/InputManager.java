package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class InputManager {
	
	boolean[] pressBuffer;
	float[] pressTimer;
	float globalTime = 0;
	float dashDetection = 0.20f;
	
	public InputManager ()
	{
		pressBuffer = new boolean[11];
		pressBuffer[0] = false; //q -> a
		pressBuffer[1] = false; //d
		pressBuffer[2] = false; //left -> k
		pressBuffer[3] = false; //right -> ;
		pressBuffer[4] = false; //space
		pressBuffer[5] = false; //z
		pressBuffer[6] = false; //s
		pressBuffer[7] = false; //up
		pressBuffer[8] = false; //down
		pressBuffer[9] = false; //enter
		pressBuffer[10] = false; //esc
		
		pressTimer = new float[11];
		pressTimer[0] = 0; //q -> a
		pressTimer[1] = 0; //d
		pressTimer[2] = 0; //left -> k
		pressTimer[3] = 0; //right -> ;
		pressTimer[4] = 0; //space
		pressTimer[5] = 0; //z
		pressTimer[6] = 0; //s
		pressTimer[7] = 0; //up
		pressTimer[8] = 0; //down	
		pressTimer[9] = 0; //enter
		pressTimer[10] = 0; //esc
	}
	
	public void CheckStartInput (RGBTrip game)
	{
		if(TouchDown(Keys.LEFT, 2, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveStartMenu(-1, 0);
			
		}
		if(TouchDown(Keys.RIGHT, 3, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveStartMenu(1, 0);
			
		}		
		if(TouchDown(Keys.UP, 7, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveStartMenu(0, -1);
			
		}
		if(TouchDown(Keys.DOWN, 8, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveStartMenu(0, 1);
			
		}
		
		if(TouchDown(Keys.ENTER, 9, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.PressEnterMenu();
			
		}
	}
	
	public void CheckInputEndGame(RGBTrip game)
	{
		if(TouchDown(Keys.LEFT, 2, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveEndMenu(-1);
		}
		if(TouchDown(Keys.RIGHT, 3, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.MoveEndMenu(1);
		}
		
		if(TouchDown(Keys.ENTER, 9, this.pressBuffer))
		{
			GameManager.Instance.PlaySelect();
			game.PressEnterEndGame();
		}		
	}
	
	public void CheckInput (RGBTrip game) 
	{
		this.globalTime += Gdx.graphics.getDeltaTime();
		
		////
		if(TouchDown(Keys.A, 0, this.pressBuffer))
		{
			float currentTimer = this.globalTime;
			
			System.out.println("time " + currentTimer);
			
			if((currentTimer - pressTimer[0]) < this.dashDetection) {
				game.DashShipOne(-1, 0);
				pressTimer[0] = 0;
			}
			else
			{
				pressTimer[0] = currentTimer;
			}
			
			pressTimer[1] = 0;
		}		
		////
		
		////
		if(TouchDown(Keys.D, 1, this.pressBuffer))
		{
			float currentTimer = this.globalTime;
			if((currentTimer - pressTimer[1]) < this.dashDetection) {
				game.DashShipOne(1, 0);
				pressTimer[1] = 0;
			}
			else
			{
				pressTimer[1] = currentTimer;
			}
			
			pressTimer[0] = 0;
		}		
		////		
		
		////
		if(TouchDown(Keys.K, 2, this.pressBuffer))
		{
			float currentTimer = this.globalTime;
			if((currentTimer - pressTimer[2]) < this.dashDetection) {
				game.DashShipTwo(-1, 0);
				pressTimer[2] = 0;
			}
			else
			{
				pressTimer[2] = currentTimer;
			}
			
			pressTimer[3] = 0;
		}		
		////		
		
		////
		if(TouchDown(Keys.SEMICOLON, 3, this.pressBuffer))
		{
			float currentTimer = this.globalTime;
			if((currentTimer - pressTimer[3]) < this.dashDetection) {
				game.DashShipTwo(1, 0);
				pressTimer[3] = 0;
			}
			else
			{
				pressTimer[3] = currentTimer;
			}
			
			pressTimer[2] = 0;
		}		
		////		
		
		if(Gdx.input.isKeyPressed(Keys.A))
			game.MoveShipOne(-1, 0);
		
		if(Gdx.input.isKeyPressed(Keys.D))
			game.MoveShipOne(1, 0);
		
		if(Gdx.input.isKeyPressed(Keys.K))
			game.MoveShipTwo(-1, 0);
		
		if(Gdx.input.isKeyPressed(Keys.SEMICOLON))
			game.MoveShipTwo(1, 0);
		
		if(TouchDown(Keys.SPACE,4,this.pressBuffer))
		{
			game.NextColorOnBoard();
		}
		
		if(TouchDown(Keys.ESCAPE, 10, this.pressBuffer))
		{
			game.Escape();			
		}		
		
		if(Gdx.input.isKeyPressed(Keys.Z))
		{
			game.MoveShipOne(0, 1);
		}
		
		if(Gdx.input.isKeyPressed(Keys.S))
		{
			game.MoveShipOne(0, -1);
		}
		
		if(Gdx.input.isKeyPressed(Keys.UP))
		{
			game.MoveShipTwo(0, 1);
		}
		
		if(Gdx.input.isKeyPressed(Keys.DOWN))
		{
			game.MoveShipTwo(0, -1);
		}
	}
	
	public boolean TouchDown(int key, int index, boolean[] pressed)
	{
		if(!pressed[index])
		{
			if(Gdx.input.isKeyPressed(key))
			{
				pressed[index] = true;
				return true;
			}
		}
		else
		{
			if(!Gdx.input.isKeyPressed(key))
			{
				pressed[index] = false;
				return false;
			}
		}
		
		return false;
	}
}
