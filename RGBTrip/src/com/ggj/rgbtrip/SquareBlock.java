package com.ggj.rgbtrip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public class SquareBlock extends RenderObject {

	private float x, y, width, height;
	public SquareBlock(float pX, float pY, Color pColor){
		this.color = pColor;
		this.x = pX;
		this.y = pY;
		this.width = GameManager.Instance.BlockSize;
		this.height = GameManager.Instance.BlockSize;
	}
	
	@Override
	void render(ShapeRenderer shape) {
		// TODO Auto-generated method stub
		shape.setColor(this.color);
		shape.rect(this.x, this.y, this.width, this.height);
		
	}
	
	@Override
	public void Move(float pX, float pY, float pDt){
		this.x += pX * pDt * speed;
		this.y += pY * pDt * speed;
	}
	
	@Override
	public Rectangle getAABB()
	{
		return new Rectangle(this.x, this.y, this.width, this.height);
	}
}
