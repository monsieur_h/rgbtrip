package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.ggj.rgbtrip.RGBTrip.Side;

public class Ship extends RenderObject {
	private Side side;
	private SpriteBatch batch;
	private float radius;
	private float x, y;
	
	private float accelerationFactor;
	private float accelerationMax;
	private float accX;
	private float previousDirection;
	
	private float dashEffectDuration;
	private float dashEffectElapsed;
	
	private float effectDuration;
	private float effectElapsed;
	private float lastDashDirection;
	
	public Ship(float radius)
	{
		this.radius = radius;
		this.accelerationFactor = 1.55f;
		this.accelerationMax 	= 2.75f;
		this.accX 				= 1.0f;
		this.effectDuration 	= this.effectElapsed = 0.25f;//in seconds
		this.dashEffectDuration = this.dashEffectElapsed = 0.15f;
	}
	
	@Override
	public void render(ShapeRenderer shape)
	{
		
		float effectRatio = this.effectElapsed  / this.effectDuration;
		Color outline = new Color(1*effectRatio, 1*effectRatio, 1*effectRatio, 1);
		
		shape.setColor(Color.WHITE);
		shape.circle(this.x, this.y, this.radius * 1.3f / (effectRatio+1)*2);
		
		
		Color innerColor = new Color(-effectRatio+1, -effectRatio+1, -effectRatio+1, 1);
		shape.setColor(innerColor);
		shape.circle(this.x, this.y, this.radius / (effectRatio+1)*2);
		
		if(this.dashEffectDuration != this.dashEffectElapsed)
		{
			int offset = (int) (8 * this.lastDashDirection) * -1;
			float r;
			float i =1;
			while(i < 11f)
			{
				r = i / 11.0f;
				r = 1-r;				
				shape.setColor(innerColor.r, innerColor.g, innerColor.b, r );
				shape.circle(this.x + i*offset, this.y, this.radius * r );
				i++;
			}
		}
		
		this.updateEffect(Gdx.graphics.getDeltaTime());
	}
	
	@Override
	public Rectangle getAABB()
	{
		float w = this.radius/2;
		return new Rectangle(this.x + w, this.y + w, w, w);
	}
	
	public void onCollision()
	{
		//OMG I lost points !
		GameManager.Instance.playerCollision(this.side);
		this.effectElapsed = 0.0f;
	}
	
	public void setSide(Side pSide)
	{
		this.side = pSide;
	}
	
	public void setBatch(SpriteBatch batch)
	{
		this.batch = batch;
	}
	
	@Override
	public void setPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void Move(float x, float y, float dt)
	{
		if(x != 0)
		{
			if(x>0)
				this.accelerate(1);
			else
				this.accelerate(-1);
		}
		
		this.x += x * speed * dt * accX;
	}
	
	public void Dash(int x, int y)
	{
		float dt = Gdx.graphics.getDeltaTime();
		this.x += x * speed * 25 * dt ;
		this.lastDashDirection = x;
		this.dashEffectElapsed = 0.0f;
	}
	
	private void accelerate(int direction)
	{
		//Test if same way
		if(direction == this.previousDirection)
		{
			accX = (accX < 1.0f) ? 1.0f : accX;
			accX *= accelerationFactor;
			accX = (accX > accelerationMax) ? accelerationMax : accX;
		}
		else
		{
			accX = 1.0f;
		}
		this.previousDirection = direction;
	}
	
	private void updateEffect(float dt)
	{
		this.effectElapsed += dt;
		this.effectElapsed = (this.effectElapsed > effectDuration) ? effectDuration : this.effectElapsed;
		
		this.dashEffectElapsed+= dt;
		this.dashEffectElapsed= (this.dashEffectElapsed > this.dashEffectDuration) ? this.dashEffectDuration : this.dashEffectElapsed;
	}
}