package com.ggj.rgbtrip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PatternFileReader {
	
	public static String readFile(String filePath) {
		// TODO Auto-generated constructor stub
		System.out.println(filePath);
		BufferedReader br = null;
		String wholeFile = "";
		try {
 
			String sCurrentLine;
			br = new BufferedReader(new FileReader(filePath));
 
			while ((sCurrentLine = br.readLine()) != null) {
				wholeFile += sCurrentLine;
			}
			wholeFile.replace("\n", "");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return wholeFile;
	}
	
	public static ArrayList<String> getFileList(String filePath)
	{
		String files;
		File folder = new File(filePath);
		File[] listOfFiles = folder.listFiles(); 
		if(!folder.isDirectory())
		{
			
			return new ArrayList<String>();
		}
		ArrayList<String> stringList = new ArrayList<String>();
		for (int i = 0; i < listOfFiles.length; i++) 
		{
			if (listOfFiles[i].isFile()) 
			{
				files = listOfFiles[i].getName();
				if ((files.endsWith(".txt") || files.endsWith(".TXT")) 
						&& (!files.contains("color")))
				{
					files = files.replace(".txt", "");
					files = files.replace(".TXT", "");
					stringList.add(files);
				}
			}
		}
		return stringList;
	}
}
