package com.ggj.rgbtrip;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.math.MathUtils;
import com.ggj.rgbtrip.RGBTrip.Side;

public class GameManager {
	Color[] colors;
	public float scoreLeft, scoreRight;
	public float timer;
	public float maxTimer;
	public static GameManager Instance;
	
	public float BlockSize;
	
	public ArrayList<String> AllPaterns;
	
	public GameManager()
	{
		this.maxTimer = this.timer = 90f;

		this.SetGameColor();
		
		scoreLeft = scoreRight = 500f;
		GameManager.Instance = this;
		
		this.AllPaterns = PatternFileReader.getFileList(Gdx.files.internal("RGBTrip/").file().getAbsolutePath());
	}
	
	public void SetGameColor()
	{
		ArrayList<String> colorFilesName = PatternFileReader.getFileList(Gdx.files.internal("RGBTrip/Config/").file().getAbsolutePath());
		
		String colorFile = PatternFileReader.readFile( 
				Gdx.files.internal("RGBTrip/Config/" + 
						colorFilesName.get( MathUtils.random(0, colorFilesName.size()-1)) 
						+ ".txt").file().getAbsolutePath());
		
		String[] colorSplit = colorFile.split(",");
		
		colors = new Color[colorSplit.length/4];
		
		int index = 0;
		int count = 0;
		while(index < colorSplit.length)
		{
			colors[count] = new Color(Float.parseFloat(colorSplit[index])/255, Float.parseFloat(colorSplit[index+1])/255, 
					Float.parseFloat(colorSplit[index+2])/255, Float.parseFloat(colorSplit[index+3])/255);

			index+=4;
			count++;
		}		
	}
	
	public Color GetNextColor(Color color)
	{
		int index = 0;
		while(index < this.colors.length)
		{
			if(color.equals(this.colors[index]))
			{
				break;
			}
			
			index++;
		}
		
		index++;
		if(index>=this.colors.length)
		{
			index = 0;
		}
		
		return this.colors[index];
	}
	
	public Color GetRandomColor()
	{
		int index = MathUtils.random(0, this.colors.length-1);
		return this.colors[index];
	}
	

	public void playerCollision(Side pSide)
	{		
		if(pSide == Side.LEFT)
		{
			this.PlayHitSound(Side.LEFT);
			scoreLeft -= 10;
			System.out.println("Collision on left screen. New score : " + scoreLeft);
		}
		else
		{
			this.PlayHitSound(Side.RIGHT);
			scoreRight -= 10;
			System.out.println("Collision on right screen. New score : " + scoreRight);
		}
	}

	public Pattern CreatePattern()
	{
		return new Pattern();
	}
	
	public void update(float deltaTime)
	{
		this.timer -= deltaTime;
	}
	
	long musicId;
	Sound soundMusic;
	public void StartMusic()
	{
		String path = "RGBTrip/Sound/Club-Loop-3.mp3";
		
		soundMusic = Gdx.audio.newSound(Gdx.files.internal(path));
		
		musicId = soundMusic.play(1.0F);
		soundMusic.setLooping(musicId, true);
	}
	
	public void StopMusic()
	{
		soundMusic.stop(musicId);
	}
	
	public void PlayHitSound(Side side)
	{
		String path = "RGBTrip/Sound/Hit_Hurt63.wav";
	
		Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
		
		long id = sound.play(1.0F);
		
		if(side == Side.LEFT)
		{
			sound.setPan(id, -1, 1.0f);
		}
		else
		{
			sound.setPan(id, 1, 1.0f);
		}
		
	}
	
	public void PlayHitSpace()
	{
		String path = "RGBTrip/Sound/Switch.wav";
		
		Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
		
		long id = sound.play(1.0F);	
	}
	
	public void PlaySelect()
	{
		String path = "RGBTrip/Sound/Select.wav";
		
		Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
		
		long id = sound.play(1.0F);			
	}
}
