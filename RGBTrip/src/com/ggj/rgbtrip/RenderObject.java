package com.ggj.rgbtrip;

import com.badlogic.gdx.graphics.glutils.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

abstract class RenderObject {
	protected Color color;
	protected float[] vertices;
	protected float speed = 150f;
	public boolean hasCollided;
	
	abstract void render(ShapeRenderer shape);
	public void setColor(Color color){
		this.color = color;
	}
	
	protected void Move(float xOffset, float yOffset, float pDt){
		int i = 0;
		while(i<this.vertices.length)
		{
			this.vertices[i] += xOffset * pDt * speed;
			i++;
			this.vertices[i] += yOffset * pDt * speed;
			i++;
		}
	}
	
	protected void setSpeed(float pSpeed)
	{
		this.speed = pSpeed;
	}
	
	protected void setPosition(float x, float y)
	{
		Rectangle aabb = this.getAABB();
		float xOff, yOff;
		xOff = x - aabb.x;
		yOff = y - aabb.y;
		
		int i = 0;
		while(i<this.vertices.length)
		{
			this.vertices[i] += xOff;
			i++;
			this.vertices[i] += yOff;
			i++;
		}
	}
	
	public Rectangle getAABB()
	{
		Polygon p = new Polygon(vertices);
		return p.getBoundingRectangle();
	}
	
	public void onCollision()
	{
		this.hasCollided = true;
	}
}
