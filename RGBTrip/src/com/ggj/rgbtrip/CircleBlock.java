package com.ggj.rgbtrip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public class CircleBlock extends RenderObject {

	private float x, y, width;
	public CircleBlock(float pX, float pY, Color pColor){
		this.color = pColor;
		this.x = pX;
		this.y = pY;
		this.width = GameManager.Instance.BlockSize / 2;
	}
	
	@Override
	void render(ShapeRenderer shape) {
		// TODO Auto-generated method stub
		shape.setColor(this.color);
		shape.circle(this.x, this.y, this.width);		
	}
	
	@Override
	public void Move(float pX, float pY, float pDt){
		this.x += pX * pDt * speed;
		this.y += pY * pDt * speed;
	}
	
	public Rectangle getAABB()
	{
		return new Rectangle(this.x - this.width/2, this.y - this.width/2, this.width, this.width);
	}
}
