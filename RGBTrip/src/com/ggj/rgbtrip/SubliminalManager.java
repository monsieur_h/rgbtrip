package com.ggj.rgbtrip;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class SubliminalManager {
	ArrayList<Sprite> Sprites;
	ArrayList<Texture> Textures;
	
		
	private SpriteBatch batch;
	
	public int count = 1;
	
	public SubliminalManager () 
	{	
		this.Sprites = new ArrayList<Sprite>();
		this.Textures = new ArrayList<Texture>();
		
		this.Textures.add(new Texture(Gdx.files.internal("RGBTrip/Images/subliminal_2.png")));
		this.Textures.add(new Texture(Gdx.files.internal("RGBTrip/Images/subliminal_3.png")));
		this.Textures.add(new Texture(Gdx.files.internal("RGBTrip/Images/subliminal_4.png")));
		
		for(int i = 0; i < this.Textures.size(); i++)
		{
			this.Sprites.add(new Sprite(this.Textures.get(i)));
			this.Sprites.get(i).setPosition(Gdx.graphics.getWidth()/2 - this.Sprites.get(i).getWidth()/2,
											Gdx.graphics.getHeight()/2 - this.Sprites.get(i).getHeight()/2);
			this.Sprites.get(i).setSize(600f, 600f);
		}
		
		batch = new SpriteBatch();
	}
	
	public void SwitchColor()
	{
		count ++;
	}
	public void render()
	{/*
		if(count%8 == 0)
		{
			batch.begin();
			this.Sprites.get( MathUtils.random(0, this.Sprites.size()-1) ).draw(batch);
			batch.end();
        	count = 1;
		}
		*/
	}
}
