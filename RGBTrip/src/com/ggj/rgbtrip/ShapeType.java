package com.ggj.rgbtrip;

public enum ShapeType {
	SQUARE,
	RECTANGLE,
	CIRCLE,
	TRIANGLE
}
