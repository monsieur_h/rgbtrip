package com.ggj.rgbtrip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public class RectangleBlock extends RenderObject {

	private float x, y, width, height;
	
	
	public RectangleBlock(float pX, float pY, int size, int pos, Color pColor){
		this.color = pColor;
		this.x = pX;
		if(pos==0)
		{
			this.y = pY;
		}
		else
		{
			this.y = pY + GameManager.Instance.BlockSize / 2;
		}
		
		this.width = GameManager.Instance.BlockSize * size;
		this.height = GameManager.Instance.BlockSize / 2;
		this.hasCollided = false;
	}
	
	@Override
	public void render(ShapeRenderer shape) {
		shape.setColor(this.color);
		shape.rect(this.x, this.y, this.width, this.height);
	}
	
	@Override
	public void Move(float pX, float pY, float pDt){
		this.x += pX * pDt * speed;
		this.y += pY * pDt * speed;
	}
	
	@Override
	public Rectangle getAABB()
	{
		return new Rectangle(this.x, this.y, this.width, this.height);
	}
}
