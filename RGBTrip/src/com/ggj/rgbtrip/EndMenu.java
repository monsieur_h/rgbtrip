package com.ggj.rgbtrip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;

public class EndMenu {
	Sprite[] Sprites;
	Texture[] TexturesJ1;
	Texture[] TexturesJ2;
	Texture[] TexturesDraw;
	
	int i = 0;
	private SpriteBatch batch;
	private RGBTrip game;
	private BitmapFont font;
	
	public EndMenu (RGBTrip game)
	{
		this.game = game;
		
		this.Sprites = new Sprite[2];
		this.TexturesJ1 = new Texture[2];
		this.TexturesJ2 = new Texture[2];
		this.TexturesDraw = new Texture[2];
		
		Texture.setEnforcePotImages(false);
		
		this.TexturesJ1[0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Victory_J1_Retry.png"));
		this.TexturesJ1[1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Victory_J1_Quit.png"));		
		
		this.TexturesJ2[0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Victory_J2_Retry.png"));
		this.TexturesJ2[1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Victory_J2_Quit.png"));
		
		this.TexturesDraw[0] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Draw_Retry.png"));
		this.TexturesDraw[1] = new Texture(Gdx.files.internal("RGBTrip/Images/Menue/Final_Screen_Draw_Quit.png"));
		
		Texture.setEnforcePotImages(true);

		this.font = new BitmapFont(Gdx.files.internal("RGBTrip/Square-32.fnt"));
		font.scale(0.9f);
	
		
		batch = new SpriteBatch();
	}
	
	float bestScore = 0f;
	float lowScore = 0f;
	float drawScore = 0f;
	
	public void SetVictory(float score1, float score2)
	{
		bestScore = 0f;
		lowScore = 0f;
		drawScore = 0f;
		
		if(score1==score2)
		{
			this.Sprites[0] = new Sprite(this.TexturesDraw[0]);
			this.Sprites[1] = new Sprite(this.TexturesDraw[1]);		
			drawScore = score1;
		}
		else if(score1>score2)
		{
			this.Sprites[0] = new Sprite(this.TexturesJ1[0]);
			this.Sprites[1] = new Sprite(this.TexturesJ1[1]);		
			bestScore = score1;
			lowScore = score2;
		}
		else
		{
			this.Sprites[0] = new Sprite(this.TexturesJ2[0]);
			this.Sprites[1] = new Sprite(this.TexturesJ2[1]);				
			bestScore = score2;
			lowScore = score1;
		}
		
		for(int i = 0; i < this.Sprites.length; i++)
		{
			this.Sprites[i].setPosition(0, 0);
			this.Sprites[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
		
		this.i = 0;
	}
	
	public void Move(int x)
	{
		if(i+x >= 0 && i+x < this.Sprites.length)
		{
			i+=x;
		}
	}
	
	public void PressEnter()
	{
		if(i==0)
		{
			this.game.RestartGame();
		}
		else
		{
			//quit
			Gdx.app.exit();
		}
	}	
	
	public void render()
	{
		batch.begin();
		this.Sprites[i].draw(batch);
		
		if(drawScore == 0f)
		{
			font.setScale(3f);
			
			TextBounds bounds = this.font.getBounds(""+ bestScore);
			
			font.draw(batch, ""+ bestScore,
					Gdx.graphics.getWidth() /2 + bounds.width / 2.5f,
					Gdx.graphics.getHeight() /2 + bounds.height*2.3f);
			
			font.setScale(2f);
			
			bounds = this.font.getBounds(""+ lowScore);
			
			font.draw(batch, ""+ lowScore,
					Gdx.graphics.getWidth() - bounds.width * 1.1f,
					Gdx.graphics.getHeight() / 2 - bounds.height*2.25f);	
		}
		else
		{
			font.setScale(2f);
			
			TextBounds bounds = this.font.getBounds(""+ drawScore);
			
			font.draw(batch, ""+ drawScore,
					Gdx.graphics.getWidth() /2 - bounds.width * 1.1f,
					Gdx.graphics.getHeight() / 2 - bounds.height*2);
		
			
			bounds = this.font.getBounds(""+ drawScore);
			
			font.draw(batch, ""+ drawScore,
					Gdx.graphics.getWidth() - bounds.width * 1.1f,
					Gdx.graphics.getHeight() / 2 - bounds.height*2);			
		}
		batch.end();
	}	
		
}
